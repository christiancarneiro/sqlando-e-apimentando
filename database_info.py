# banco de dados
database_name = 'sie_ueb'

# tabelas principais
student_table_name = 'student'
professor_table_name = 'professor'
class_table_name = 'class'
course_table_name = 'course'

# tabelas de adjacencias
class_professor_table_name = 'class_professor'
class_student_table_name = 'class_student'
