from utils import generate_database
from database_info import database_name, student_table_name, professor_table_name, class_table_name, course_table_name, class_student_table_name, class_professor_table_name

# criação do banco de dados
generate_database.create_prograd_database(database_name)

# criação das tabelas principais (com fk)
# [respeitar a ordem, já que a criação de algumas tabelas depende da pré-existência de outras]
generate_database.create_professor_table(database_name, professor_table_name)
generate_database.create_course_table(
    database_name, professor_table_name, course_table_name)
generate_database.create_student_table(
    database_name, course_table_name, student_table_name)
generate_database.create_class_table(
    database_name, course_table_name, class_table_name)

# criação das tabelas de adjacência (com fk)
generate_database.create_class_professor_table(
    database_name, class_table_name, professor_table_name,  class_professor_table_name)
generate_database.create_class_student_table(
    database_name, class_table_name, student_table_name, class_student_table_name)
