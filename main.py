from fastapi import FastAPI, HTTPException
import uvicorn
from uuid import UUID, uuid4
from utils import database_operations, database_query, database_to_object
from database_info import database_name, student_table_name, professor_table_name, class_table_name, course_table_name, class_student_table_name, class_professor_table_name

from schemas.student import StudentDataInput, Student, StudentLite, StudentList, StudentListLite
from schemas.professor import ProfessorDataInput, Professor, ProfessorLite, ProfessorList, ProfessorListLite
from schemas.course import CourseDataInput, CourseDataOutput, CourseLite, CourseList, CourseListLite
from schemas._class import ClassDataInput, ClassDataOutput, ClassLite, ClassList, ClassListLite


app = FastAPI(title="SIE UEB")

student_tag = 'Students'
course_tag = 'Courses'
professor_tag = 'Professors'
class_tag = 'Classes'

####################################################
# Students
####################################################


@app.get(
    '/students',
    summary='Fetch all students data',
    tags=[student_tag],
    response_model=StudentList
)
def get_students():
    basic_student_list = database_query.get_all_table(
        database_name, student_table_name)
    student_list = []
    for item in basic_student_list:
        student_list.append(get_student(item['key']))
    return student_list


@app.get(
    '/students/lite',
    summary='Fetch all students lite data',
    tags=[student_tag],
    response_model=StudentListLite
)
def get_students_lite():
    basic_student_list = database_query.get_all_table(
        database_name, student_table_name)
    student_list = []
    for item in basic_student_list:
        student_list.append(get_student(item['key']))
    return student_list


@app.get(
    '/students/{_student_key}',
    summary='Fetch student data',
    tags=[student_tag],
    response_model=Student
)
def get_student(_student_key: UUID):
    student = database_to_object.to_student({'key': _student_key})
    if student is None:
        raise HTTPException(
            status_code=404, detail=f'Student with key={_student_key} not found')
    student['course'] = get_course(
        student['course_key']) if student['course_key'] else None
    basic_student_classes = database_query.get_class_by_students(
        database_name, class_student_table_name, {'key': str(_student_key)})
    if basic_student_classes:
        student['classes'] = []
        for item in basic_student_classes:
            student['classes'].append(get_class(item['class_key']))
    return student


@app.get(
    '/students/{_student_key}/lite',
    summary='Fetch student lite data',
    tags=[student_tag],
    response_model=StudentLite
)
def get_student_lite(_student_key: UUID):
    student = database_to_object.to_student({'key': _student_key})
    if student is None:
        raise HTTPException(
            status_code=404, detail=f'Student with key={_student_key} not found')
    student['course'] = get_course(
        student['course_key']) if student['course_key'] else None
    basic_student_classes = database_query.get_class_by_students(
        database_name, class_student_table_name, {'key': str(_student_key)})
    if basic_student_classes:
        student['classes'] = []
        for item in basic_student_classes:
            student['classes'].append(get_class(item['class_key']))
    return student


@app.post(
    '/students',
    summary='Add a new student',
    tags=[student_tag],
    status_code=201,
    response_model=Student
)
def add_student(_student_data: StudentDataInput):
    student_key: UUID = uuid4()
    student_object = {'key': str(student_key), **_student_data.dict()}
    database_operations.add_student(
        database_name, student_table_name, student_object)
    return get_student(student_key)


@app.put(
    '/students/{_student_key}/course/{_course_key}',
    summary='Assign course to student',
    tags=[student_tag],
    status_code=201,
    response_model=Student
)
def set_student_course(_student_key: UUID, _course_key: UUID):
    get_course(_course_key)
    database_operations.assign_course_to_student(
        database_name, student_table_name, {'student_key': str(_student_key), 'course_key': str(_course_key)})
    return get_student(_student_key)


@app.post(
    '/students/{_student_key}/classes/{_class_key}',
    summary='Assign class to student',
    tags=[student_tag],
    status_code=201,
    response_model=Student
)
def set_student_class(_student_key: UUID, _class_key: UUID):
    get_class(_class_key)
    database_operations.assign_student_to_class(
        database_name, class_student_table_name, {'student_key': str(_student_key), 'class_key': str(_class_key)})
    return get_student(_student_key)


@app.delete(
    '/students/{_student_key}/classes/{_class_key}',
    summary='Remove class from student',
    tags=[student_tag, class_tag],
    status_code=201,
    response_model=Student
)
def remove_student_class(_student_key: UUID, _class_key: UUID):
    get_class(_class_key)
    database_operations.remove_class_from_student(
        database_name, class_student_table_name, {'student_key': str(_student_key), 'class_key': str(_class_key)})
    return get_student(_student_key)


####################################################
# Professors
####################################################


@app.get(
    '/professors',
    summary='Fetch all professors data',
    tags=[professor_tag],
    response_model=ProfessorList
)
def get_professors():
    basic_professor_list = database_query.get_all_table(
        database_name, professor_table_name)
    professor_list = []
    for item in basic_professor_list:
        professor_list.append(get_professor(item['key']))
    return professor_list


@app.get(
    '/professors/lite',
    summary='Fetch all professors lite data',
    tags=[professor_tag],
    response_model=ProfessorListLite
)
def get_professors_lite():
    basic_professor_list = database_query.get_all_table(
        database_name, professor_table_name)
    professor_list = []
    for item in basic_professor_list:
        professor_list.append(get_professor(item['key']))
    return professor_list


@app.get(
    '/professor/{_professor_key}',
    summary='Fetch professor data',
    tags=[professor_tag],
    response_model=Professor
)
def get_professor(_professor_key: UUID):
    professor = database_to_object.to_professor({'key': _professor_key})
    if professor is None:
        raise HTTPException(
            status_code=404, detail=f'Professor with key={_professor_key} not found')
    return professor


@app.get(
    '/professor/{_professor_key}/lite',
    summary='Fetch professor lite data',
    tags=[professor_tag],
    response_model=ProfessorLite
)
def get_professor_lite(_professor_key: UUID):
    professor = database_to_object.to_professor({'key': _professor_key})
    if professor is None:
        raise HTTPException(
            status_code=404, detail=f'Professor with key={_professor_key} not found')
    return professor


@ app.post(
    '/professors',
    summary='Add a new professor',
    tags=[professor_tag],
    status_code=201,
    response_model=Professor
)
def add_professor(_professor_data: ProfessorDataInput):
    professor_key: UUID = uuid4()
    professor_object = {'key': str(professor_key), **_professor_data.dict()}
    database_operations.add_professor(
        database_name, professor_table_name, professor_object)
    return get_professor(professor_key)


@app.post(
    '/professors/{_professor_key}/classes/{_class_key}',
    summary='Assign class to professor',
    tags=[professor_tag],
    status_code=201,
    response_model=Professor
)
def set_professor_class(_professor_key: UUID, _class_key: UUID):
    get_class(_class_key)
    database_operations.assign_professor_to_class(
        database_name, class_professor_table_name, {'professor_key': str(_professor_key), 'class_key': str(_class_key)})
    return get_professor(_professor_key)


@app.delete(
    '/professors/{_professor_key}/classes/{_class_key}',
    summary='Remove class from professor',
    tags=[professor_tag],
    status_code=201,
    response_model=Professor
)
def remove_professor_class(_professor_key: UUID, _class_key: UUID):
    get_class(_class_key)
    database_operations.remove_class_from_professor(
        database_name, class_professor_table_name, {'professor_key': str(_professor_key), 'class_key': str(_class_key)})
    return get_professor(_professor_key)
####################################################
# Courses
####################################################


@app.get(
    '/course',
    summary='Fetch all courses data',
    tags=[course_tag],
    response_model=CourseList
)
def get_courses():
    basic_course_list = database_query.get_all_table(
        database_name, course_table_name)
    course_list = []
    for item in basic_course_list:
        course_list.append(get_course(item['key']))
    return course_list


@app.get(
    '/course/lite',
    summary='Fetch all courses lite data',
    tags=[course_tag],
    response_model=CourseListLite
)
def get_courses_lite():
    basic_course_list = database_query.get_all_table(
        database_name, course_table_name)
    course_list = []
    for item in basic_course_list:
        course_list.append(get_course(item['key']))
    return course_list


@app.get(
    '/course/{_course_key}',
    summary='Fetch course data',
    tags=[course_tag],
    response_model=CourseDataOutput
)
def get_course(_course_key: UUID):
    course = database_to_object.to_course({'key': _course_key})
    if course is None:
        raise HTTPException(
            status_code=404, detail=f'Course with key={_course_key} not found')
    course['coordinator'] = get_professor(course['coordinator_key'])
    return course


@app.get(
    '/course/{_course_key}/lite',
    summary='Fetch course lite data',
    tags=[course_tag],
    response_model=CourseLite
)
def get_course_lite(_course_key: UUID):
    course = database_to_object.to_course({'key': _course_key})
    if course is None:
        raise HTTPException(
            status_code=404, detail=f'Course with key={_course_key} not found')
    course['coordinator'] = get_professor(course['coordinator_key'])
    return course


@ app.post(
    '/courses',
    summary='Add a new course',
    tags=[course_tag],
    status_code=201,
    response_model=CourseDataOutput
)
def add_course(_course_data: CourseDataInput):
    course_data_dict = _course_data.dict()
    get_professor(course_data_dict['coordinator_key'])
    course_key: UUID = uuid4()
    course_object = {'key': str(course_key), **course_data_dict}
    database_operations.add_course(
        database_name, course_table_name, course_object)
    return get_course(course_key)


@app.get(
    '/courses/{_course_key}',
    summary='Fetch student data by course',
    tags=[course_tag],
    response_model=StudentListLite
)
def get_student_by_course(_course_key: UUID):
    students = database_query.get_student_by_course(
        database_name, student_table_name, {'key': str(_course_key)})
    if students is None or students == []:
        raise HTTPException(
            status_code=404, detail=f'Students of course with key={_course_key} not found')
    student_list = []
    for item in students:
        student_list.append(get_student(item['key']))
    return student_list
####################################################
# Classes
####################################################


@app.get(
    '/classes',
    summary='Fetch all classes data',
    tags=[class_tag],
    response_model=ClassList
)
def get_classes():
    basic_class_list = database_query.get_all_table(
        database_name, class_table_name)
    class_list = []
    for item in basic_class_list:
        class_list.append(get_class(item['key']))
    return class_list


@app.get(
    '/classes/lite',
    summary='Fetch all classes lite data',
    tags=[class_tag],
    response_model=ClassListLite
)
def get_classes_lite():
    basic_class_list = database_query.get_all_table(
        database_name, class_table_name)
    class_list = []
    for item in basic_class_list:
        class_list.append(get_class(item['key']))
    return class_list


@app.get(
    '/class/{_class_key}',
    summary='Fetch class data',
    tags=[class_tag],
    response_model=ClassDataOutput
)
def get_class(_class_key: UUID):
    _class = database_to_object.to_class({'key': _class_key})
    if _class is None:
        raise HTTPException(
            status_code=404, detail=f'Class with key={_class_key} not found')
    _class['course'] = get_course(_class['course_key'])
    return _class


@app.get(
    '/class/{_class_key}/lite',
    summary='Fetch class lite data',
    tags=[class_tag],
    response_model=ClassLite
)
def get_class_lite(_class_key: UUID):
    _class = database_to_object.to_class({'key': _class_key})
    if _class is None:
        raise HTTPException(
            status_code=404, detail=f'Class with key={_class_key} not found')
    _class['course'] = get_course(_class['course_key'])
    return _class


@ app.post(
    '/classes',
    summary='Add a new class',
    tags=[class_tag],
    status_code=201,
    response_model=ClassDataOutput
)
def add_class(_class_data: ClassDataInput):
    class_data_dict = _class_data.dict()
    get_course(class_data_dict['course_key'])
    class_key: UUID = uuid4()
    class_object = {'key': str(class_key), **class_data_dict}
    database_operations.add_class(
        database_name, class_table_name, class_object)
    return get_class(class_key)


@app.get(
    '/classes/{_class_key}',
    summary='Fetch student data by class',
    tags=[class_tag],
    response_model=StudentListLite
)
def get_student_by_class(_class_key: UUID):
    students = database_query.get_person_key_by_class(
        database_name, class_student_table_name, {'key': str(_class_key)})
    if students is None or students == []:
        raise HTTPException(
            status_code=404, detail=f'Students of class with key={_class_key} not found')
    student_list = []
    for item in students:
        student_list.append(get_student(item['student_key']))
    return student_list


@app.get(
    '/classes/ranking/bystudents',
    summary='Fetch classes data by order of enrolled students',
    tags=[class_tag],
    response_model=ClassListLite
)
def get_classes_ranking():
    basic_class_list = database_query.get_class_ranking(
        database_name, class_student_table_name)
    class_list = []
    for item in basic_class_list:
        class_list.append(get_class(item['class_key']))
    return class_list


if __name__ == '__main__':
    uvicorn.run(app='main:app', port=3000, reload=True)
