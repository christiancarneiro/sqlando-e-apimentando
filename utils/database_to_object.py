from schemas import professor, student, course, _class
from utils import database_query
from database_info import database_name, student_table_name, professor_table_name, class_table_name, course_table_name, class_student_table_name, class_professor_table_name


def to_student(_database_key: dict):
    student_data = database_query.get_object_by_key(
        database_name, student_table_name, _database_key)
    if student_data:
        base_dict = dict(student_data)
        base_dict['rg'] = student.IdentityCard.parse_obj(student_data)
        return base_dict


def to_professor(_database_key: dict):
    return database_query.get_object_by_key(
        database_name, professor_table_name, _database_key)


def to_course(_database_key: dict):
    course_data = database_query.get_object_by_key(
        database_name, course_table_name, _database_key)
    if course_data:
        return dict(course_data)


def to_class(_database_key: dict):
    class_data = database_query.get_object_by_key(
        database_name, class_table_name, _database_key)
    if class_data:
        return dict(class_data)
