import sqlite3


def create_prograd_database(_database_name):  # create the database
    sqlite3.connect(_database_name + '.db')


# cria a tabela de professores
def create_professor_table(_database_name, _table_name):
    database = sqlite3.connect(_database_name + '.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f'CREATE TABLE {_table_name} (key VARCHAR(36) PRIMARY KEY NOT NULL CHECK (LENGTH(KEY) == 36), cpf VARCHAR(14) NOT NULL UNIQUE, name VARCHAR(255) NOT NULL, title VARCHAR (255) NOT NULL)')


# cria a tabela de cursos
def create_course_table(_database_name, _professor_table_name, _table_name):
    database = sqlite3.connect(_database_name + '.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f'CREATE TABLE {_table_name} (key VARCHAR(36) PRIMARY KEY NOT NULL CHECK (LENGTH(KEY) == 36), name VARCHAR(255) NOT NULL, creation_date VARCHAR(10) NOT NULL, academic_unity VARCHAR(255) NOT NULL, coordinator_key VARCHAR(36) UNIQUE, FOREIGN KEY(COORDINATOR_KEY) REFERENCES {_professor_table_name}(key) ON DELETE SET NULL)')


# cria a tabela de estudantes
def create_student_table(_database_name, _course_table_name, _table_name):
    database = sqlite3.connect(_database_name + '.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f'CREATE TABLE {_table_name} (key VARCHAR(36) PRIMARY KEY NOT NULL CHECK (LENGTH(KEY) == 36), cpf VARCHAR(14) NOT NULL UNIQUE, name VARCHAR(255) NOT NULL, rg_number VARCHAR(9) NOT NULL, issuing_agency VARCHAR(55) NOT NULL, issuing_agency_state VARCHAR(2) NOT NULL, birth_date VARCHAR(10) NOT NULL, course_key VARCHAR(36), UNIQUE(rg_number, issuing_agency, issuing_agency_state), FOREIGN KEY(course_key) REFERENCES {_course_table_name}(key) ON DELETE SET NULL)')


# cria a tabela de matérias (sem considerar FK)
def create_class_table(_database_name, _course_table_name, _table_name):
    database = sqlite3.connect(_database_name + '.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f'CREATE TABLE {_table_name} (key VARCHAR(36) PRIMARY KEY NOT NULL CHECK (LENGTH(KEY) == 36), name VARCHAR(255) NOT NULL, course_key VARCHAR(36) NOT NULL, code VARCHAR(55) UNIQUE NOT NULL, description TEXT, workload INT NOT NULL, FOREIGN KEY(course_key) REFERENCES {_course_table_name}(key) ON DELETE CASCADE)')


# cria a tabela de (adjacências) mxn para estudantes e disciplinas
def create_class_student_table(_database_name, _class_table_name, _student_table_name, _table_name):
    database = sqlite3.connect(_database_name + '.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f'CREATE TABLE {_table_name} (class_key VARCHAR(36) NOT NULL, student_key VARCHAR(36) NOT NULL, FOREIGN KEY(class_key) REFERENCES {_class_table_name}(key) ON DELETE CASCADE, FOREIGN KEY(student_key) REFERENCES {_student_table_name}(key) ON DELETE CASCADE, UNIQUE(class_key, student_key))')


# cria a tabela de (adjacências) mxn para professores e disciplinas
def create_class_professor_table(_database_name, _class_table_name, _professor_table_name, _table_name):
    database = sqlite3.connect(_database_name + '.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f'CREATE TABLE {_table_name} (class_key VARCHAR(36) NOT NULL, professor_key VARCHAR(36) NOT NULL, FOREIGN KEY(class_key) REFERENCES {_class_table_name}(key) ON DELETE CASCADE, FOREIGN KEY(professor_key) REFERENCES {_professor_table_name}(key) ON DELETE CASCADE, UNIQUE(class_key, professor_key))')


# O sqlite não porta ALTER TABLE ADD CONSTRAINT. IGNORAR ABAIXO --------------------------------------------------.
# relaciona a coluna course_ key da tabela de estudantes à key da tabela de cursos como foreign key
def attach_student_course(_database_name, _student_table_name, _course_table_name):
    database = sqlite3.connect(_database_name + '.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f'ALTER TABLE {_student_table_name} ADD FOREIGN KEY(COURSE_KEY) REFERENCES {_course_table_name}(KEY)')


# relaciona a coluna course_key da tabela de disciplinas à key da tabela de cursos como foreign key
def attach_class_course(_database_name, _class_table_name, _course_table_name):
    database = sqlite3.connect(_database_name + '.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f'ALTER TABLE {_class_table_name} ADD FOREIGN KEY(COURSE_KEY) REFERENCES {_course_table_name}(KEY)')


# relaciona a coluna coordinator_key da tabela de cursos à key da tabela de professores como foreign key
def attach_course_professor(_database_name, _class_table_name, _professor_table_name):
    database = sqlite3.connect(_database_name + '.db')
    database_cursor = database.cursor()
    database_cursor.execute(
        f'ALTER TABLE {_class_table_name} ADD FOREIGN KEY COORDINATOR_KEY REFERENCES {_professor_table_name}(KEY)')
