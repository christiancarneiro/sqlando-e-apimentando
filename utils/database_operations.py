import sqlite3


def add_student(_database_name, _table_name, _student_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_student_object['key'], _student_object['cpf'], _student_object['name'],
            _student_object['rg']['rg_number'], _student_object['rg']['issuing_agency'], _student_object['rg']['issuing_agency_state'], _student_object['birth_date'])
    database_cursor.execute(f'PRAGMA foreign_keys = ON;')
    database_cursor.execute(
        f'INSERT INTO {_table_name} (key, cpf, name, rg_number, issuing_agency, issuing_agency_state, birth_date) VALUES (?, ?, ?, ?, ?, ?, ?)', data)
    database.commit()
    database.close()


def add_course(_database_name, _table_name, _course_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_course_object['key'], _course_object['name'], _course_object['creation_date'],
            _course_object['academic_unity'], _course_object['coordinator_key'])
    database_cursor.execute(f'PRAGMA foreign_keys = ON;')
    database_cursor.execute(
        f'INSERT INTO {_table_name} (key, name, creation_date, academic_unity, coordinator_key) VALUES (?, ?, ?, ?, ?)', data)
    database.commit()
    database.close()


def add_professor(_database_name, _table_name, _professor_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_professor_object['key'], _professor_object['cpf'], _professor_object['name'],
            _professor_object['title'])
    database_cursor.execute(f'PRAGMA foreign_keys = ON;')
    database_cursor.execute(
        f'INSERT INTO {_table_name} (key, cpf, name, title) VALUES (?, ?, ?, ?)', data)
    database.commit()
    database.close()


def add_class(_database_name, _table_name, _class_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_class_object['key'], _class_object['name'], _class_object['course_key'],
            _class_object['code'], _class_object['description'], _class_object['workload'])
    database_cursor.execute(f'PRAGMA foreign_keys = ON;')
    database_cursor.execute(
        f'INSERT INTO {_table_name} (key, name, course_key, code, description, workload) VALUES (?, ?, ?, ?, ?, ?)', data)
    database.commit()
    database.close()


def assign_student_to_class(_database_name, _table_name, _student_class_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_student_class_object['class_key'],
            _student_class_object['student_key'])
    database_cursor.execute(f'PRAGMA foreign_keys = ON;')
    database_cursor.execute(
        f'INSERT INTO {_table_name} (class_key, student_key) VALUES (?, ?)', data)
    database.commit()
    database.close()


def assign_professor_to_class(_database_name, _table_name, _professor_class_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_professor_class_object['class_key'],
            _professor_class_object['professor_key'])
    database_cursor.execute(f'PRAGMA foreign_keys = ON;')
    database_cursor.execute(
        f'INSERT INTO {_table_name} (class_key, professor_key) VALUES (?, ?)', data)
    database.commit()
    database.close()


def assign_course_to_student(_database_name, _table_name, _student_course_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_student_course_object['course_key'],
            _student_course_object['student_key'])
    database_cursor.execute(f'PRAGMA foreign_keys = ON;')
    database_cursor.execute(
        f'UPDATE {_table_name} SET course_key=(?) WHERE key=(?)', data)
    database.commit()
    database.close()


def remove_class_from_student(_database_name, _table_name, _student_class_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_student_class_object['student_key'],
            _student_class_object['class_key'])
    database_cursor.execute(f'PRAGMA foreign_keys = ON;')
    database_cursor.execute(
        f'DELETE FROM {_table_name} WHERE student_key=(?) and class_key=(?)', data)
    database.commit()
    database.close()


def remove_class_from_professor(_database_name, _table_name, _professor_class_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_professor_class_object['professor_key'],
            _professor_class_object['class_key'])
    database_cursor.execute(f'PRAGMA foreign_keys = ON;')
    database_cursor.execute(
        f'DELETE FROM {_table_name} WHERE professor_key=(?) and class_key=(?)', data)
    database.commit()
    database.close()
