import sqlite3


def get_object_by_key(_database_name, _table_name, _key_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    data = (str(_key_object["key"]),)
    database_cursor.execute(
        f'SELECT * FROM {_table_name} WHERE key=(?)', data)
    response = database_cursor.fetchone()
    if not response:
        print('Não foi possível obter uma resposta com esses dados')
    database.close()
    return response


def get_student_key_by_cpf(_database_name, _table_name, _cpf_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_cpf_object['cpf'],)
    database_cursor.execute(
        f'SELECT KEY FROM {_table_name} WHERE cpf=(?)', data)
    response = database_cursor.fetchone()[0]
    if not response:
        print('Não foi possível obter um estudante com esses dados')
    database.close()
    return response


def get_student_by_cpf(_database_name, _table_name, _cpf_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    data = (_cpf_object['cpf'],)
    database_cursor.execute(
        f'SELECT * FROM {_table_name} WHERE cpf=(?)', data)
    response = database_cursor.fetchone()
    if not response:
        print('Não foi possível obter um estudante com esses dados')
    database.close()
    return response


def get_professor_key_by_cpf(_database_name, _table_name, _cpf_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_cpf_object['cpf'],)
    database_cursor.execute(
        f'SELECT KEY FROM {_table_name} WHERE cpf=(?)', data)
    response = database_cursor.fetchone()[0]
    if not response:
        print('Não foi possível obter um professor com esses dados')
    database.close()
    return response


def get_professor_by_cpf(_database_name, _table_name, _cpf_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    data = (_cpf_object['cpf'],)
    database_cursor.execute(
        f'SELECT * FROM {_table_name} WHERE cpf=(?)', data)
    response = database_cursor.fetchone()
    if not response:
        print('Não foi possível obter um professor com esses dados')
    database.close()
    return response


def get_course_key_by_name_unity(_database_name, _table_name, _data_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_data_object["name"], _data_object["academic_unity"])
    database_cursor.execute(
        f'SELECT KEY FROM {_table_name} WHERE name=(?) AND academic_unity=(?)', data)
    response = database_cursor.fetchone()[0]
    if not response:
        print('Não foi possível obter um curso com esses dados')
    database.close()
    return response


def get_course_by_name_unity(_database_name, _table_name, _data_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    data = (_data_object["name"], _data_object["academic_unity"])
    database_cursor.execute(
        f'SELECT * FROM {_table_name} WHERE name=(?) AND academic_unity=(?)', data)
    response = database_cursor.fetchone()
    if not response:
        print('Não foi possível obter um curso com esses dados')
    database.close()
    return response


def get_class_key_by_code(_database_name, _table_name, _code_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database_cursor = database.cursor()
    data = (_code_object['code'],)
    database_cursor.execute(
        f'SELECT KEY FROM {_table_name} WHERE code=(?)', data)
    response = database_cursor.fetchone()[0]
    if not response:
        print('Não foi possível obter uma disciplina com esses dados')
    database.close()
    return response


def get_class_by_code(_database_name, _table_name, _code_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    data = (_code_object['code'],)
    database_cursor.execute(
        f'SELECT * FROM {_table_name} WHERE code=(?)', data)
    response = database_cursor.fetchone()
    if not response:
        print('Não foi possível obter uma disciplina com esses dados')
    database.close()
    return response


def get_all_table(_database_name, _table_name):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    database_cursor.execute(f'SELECT * FROM {_table_name}')
    response = database_cursor.fetchall()
    if not response:
        print('Não foi possível obter uma resposta com estes dados')
    database.close()
    return response


def get_student_by_course(_database_name, _table_name, _course_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    data = (_course_object['key'],)
    database_cursor.execute(
        f'SELECT * FROM {_table_name} WHERE course_key=(?)', data)
    response = database_cursor.fetchall()
    if not response:
        print('Não foi possível obter um estudante com estes dados')
    database.close()
    return response


def get_person_key_by_class(_database_name, _table_name, _class_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    data = (_class_object['key'],)
    database_cursor.execute(
        f'SELECT * FROM {_table_name} WHERE class_key=(?)', data)
    response = database_cursor.fetchall()
    if not response:
        print('Não foi possível obter uma resposta com estes dados')
    database.close()
    return response


def get_class_by_students(_database_name, _table_name, _student_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    data = (_student_object['key'],)
    database_cursor.execute(
        f'SELECT * FROM {_table_name} WHERE student_key=(?)', data)
    response = database_cursor.fetchall()
    if not response:
        print('Não foi possível encontrar uma disciplina com estes dados')
    database.close()
    return response


def get_class_by_professor(_database_name, _table_name, _class_object: dict):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    data = (_class_object['key'],)
    database_cursor.execute(
        f'SELECT * FROM {_table_name} WHERE professor_key=(?)', data)
    response = database_cursor.fetchall()
    if not response:
        print('Não foi possível encontrar uma disciplina com estes dados')
    database.close()
    return response


def get_class_ranking(_database_name, _table_name):
    database = sqlite3.connect(_database_name+'.db')
    database.row_factory = sqlite3.Row
    database_cursor = database.cursor()
    database_cursor.execute(
        f'SELECT class_key, count(class_key) FROM {_table_name} GROUP BY class_key ORDER BY COUNT(class_key) DESC;')
    response = database_cursor.fetchall()
    if not response:
        print('Não foi possível encontrar uma disciplina')
    database.close()
    return response
