from pydantic import BaseModel, Field
from .generic import GenericModel
from typing import List
from .course import CourseLite


class ClassDataBasic(GenericModel):
    name: str = Field(title="Class name")
    code: str = Field(title="Class code")
    description: str = Field(title="Class description")
    workload: int = Field(title="Class workload (hours)")

    class Config:
        schema_extra = {
            "example": {
                "name": "Mecânica dos sólidos II",
                "code": "ECIV 203",
                "description": "Essa é difícil!",
                "workload": 60,
            }
        }


class ClassDataInput(ClassDataBasic):
    course_key: str = Field(title="Course key")

    class Config:
        schema_extra = {
            "example": {
                "name": "Mecânica dos sólidos II",
                "code": "ECIV 203",
                "description": "Essa é difícil!",
                "workload": 60,
                "course_key": "81f580cc-4e30-4b8c-8a10-5f25969eaa61",
            }
        }


class ClassDataOutput(ClassDataBasic):
    key: str = Field(title="Class key")
    course: CourseLite | None = Field(title="Class course")

    class Config:
        schema_extra = {
            "example": {
                "key": "ecac7712-08e4-4bfb-a692-9028a74428fa",
                "name": "Mecânica dos sólidos II",
                "code": "ECIV 203",
                "description": "Essa é difícil!",
                "workload": 60,
                "course": {
                    "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                    "name": "Engenharia Civil",
                    "academic_unity": "CTEC",
                }
            }
        }


class ClassLite(GenericModel):
    key: str = Field(title="Course key")
    name: str = Field(title="Class name")
    code: str = Field(title="Class code")
    course: CourseLite = Field(title="Class Course")

    class Config:
        schema_extra = {
            "example": {
                "key": "ecac7712-08e4-4bfb-a697-9028a74428fa",
                "name": "Mecânica dos sólidos II",
                "code": "ECIV 203",
                "course": {
                    "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                    "name": "Engenharia Civil",
                    "academic_unity": "CTEC",
                }
            }
        }


class ClassList(BaseModel):
    __root__: List[ClassDataOutput]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of classes'
        }


class ClassListLite(BaseModel):
    __root__: List[ClassLite]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'Lite list of classes'
        }
