from pydantic import BaseModel, Field
from .generic import GenericModel
from typing import List


class ProfessorDataInput(GenericModel):
    name: str = Field(title="Professor name")
    cpf: str = Field(title="Professor CPF",
                     regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    title: str = Field(title='Professor title')

    class Config:
        schema_extra = {
            "example": {
                "name": "Billy Jean",
                "cpf": "000.000.000-01",
                "title": "Doctor"
            }
        }


class ProfessorDataOutput(ProfessorDataInput):
    key: str = Field(title="Professor key")

    class Config:
        schema_extra = {
            "example": {
                "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                "name": "Billy Jean",
                "cpf": "000.000.000-01",
                "title": "Doctor"
            }
        }


class ProfessorLite(GenericModel):
    key: str = Field(title="Professor key")
    name: str = Field(title="Professor name")
    cpf: str = Field(title="Professor CPF",
                     regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')

    class Config:
        schema_extra = {
            "example": {
                "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                "name": "Billy Jean",
                "cpf": "000.000.000-01",
            }
        }


class Professor(ProfessorDataOutput):

    classes: list[dict] | None = Field(title='Professor classes')

    class Config:
        schema_extra = {
            "example": {
                "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                "name": "Billy Jean",
                "cpf": "000.000.000-01",
                "title": "Doctor",
                "classes": [
                    {
                        "key": "f3a9f393-3d58-4082-a8b8-cd89a63465fe",
                        "name": "Cálculo 1",
                        "code": "MAT-01",
                        "course": {
                            "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                            "name": "Engenharia Civil",
                            "academic_unity": "CTEC",
                        }
                    },
                    {
                        "key": "c97aa5a6-6284-4613-83b1-27d97d45e6e7",
                        "name": "Mecânica dos Sólidos",
                        "code": "ECIV-51",
                        "course": {
                            "key": "ecac7712-08e4-4b2b-a697-9018a74428fa",
                            "name": "Engenharia Civil",
                            "academic_unity": "CTEC",
                        }
                    }]
            }
        }


class ProfessorList(BaseModel):
    __root__: List[Professor]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of professors'
        }


class ProfessorListLite(BaseModel):
    __root__: List[ProfessorLite]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'Lite list of professors'
        }
