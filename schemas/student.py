from pydantic import BaseModel, Field
from .generic import GenericModel
from datetime import date
from typing import List
from .course import CourseLite
from ._class import ClassListLite


class IdentityCard(GenericModel):
    rg_number: str = Field(title='Number of identity card')
    issuing_agency: str = Field(title='Issuing agency of identity card')
    issuing_agency_state: str = Field(title='State of Issuing agency',
                                      min_length=2, max_length=2)

    class Config:
        schema_extra = {
            "example": {
                "rg_number": "0000000000000",
                "issuing_agency": "SSP",
                "issuint_agency_state":  "AL"
            }
        }


class StudentLite(GenericModel):
    name: str = Field(title='Student name')
    cpf: str = Field(title="Student CPF",
                     regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    key: str = Field(title='Student key')

    class Config:
        schema_extra = {
            "example": {
                "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                "name": "Michael Jackson",
                "cpf":  "000.000.000-00",
            }
        }


class StudentDataInput(GenericModel):
    name: str = Field(title='Student name')
    birth_date: date | None = Field(default=None, title='Student birth date')
    rg: IdentityCard | None = Field(title='Student identity card')
    cpf: str = Field(title="Student CPF",
                     regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')

    class Config:
        schema_extra = {
            "example": {
                "name": "Michael Jackson",
                "birth_date": "2000-01-01",
                "cpf":  "000.000.000-00",
                "rg": {
                    "rg_number": "0000000000000",
                    "issuing_agency": "SSP",
                    "issuing_agency_state":  "AL"
                }
            }
        }


class StudentDataOutput(StudentDataInput):
    key: str = Field(title='Student key')

    class Config:
        schema_extra = {
            "example": {
                "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                "name": "Michael Jackson",
                "birth_date": "2000-01-01",
                "cpf":  "000.000.000-00",
                "rg": {
                    "rg_number": "0000000000000",
                    "issuing_agency": "SSP",
                    "issuing_agency_state":  "AL"
                }
            }
        }


class Student(StudentDataOutput):
    course: CourseLite | None = Field(title='Course of the student')
    classes: ClassListLite | None = Field(title='Classes of the student')


class StudentList(BaseModel):
    __root__: List[Student]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of students'
        }


class StudentListLite(BaseModel):
    __root__: List[StudentLite]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'Lite list of students'
        }
