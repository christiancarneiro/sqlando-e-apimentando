from pydantic import BaseModel, Field
from .generic import GenericModel
from typing import List
from datetime import date
from .professor import ProfessorLite


class CourseDataBasic(GenericModel):
    name: str = Field(title="Course name")
    academic_unity: str = Field(title="Course academic unity")
    creation_date: date = Field(title="Course creation date")

    class Config:
        schema_extra = {
            "example": {
                "name": "Engenharia Civil",
                "academic_unity": "CTEC",
                "creation_date": "2000-01-01",
            }
        }


class CourseDataOutput(CourseDataBasic):
    key: str = Field(title="Course key")
    coordinator: ProfessorLite | None = Field(title="Course coordinator")

    class Config:
        schema_extra = {
            "example": {
                "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                "name": "Engenharia Civil",
                "academic_unity": "CTEC",
                "creation_date": "2000-01-01",
                "coordinator": {
                    "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                    "name": "Billy Jean",
                    "cpf": "000.000.000-01",
                    "title": "Doctor"
                }
            }
        }


class CourseLite(CourseDataBasic):
    key: str = Field(title="Course key")
    name: str = Field(title="Course name")
    academic_unity: str = Field(title="Course academic unity")
    coordinator: ProfessorLite | None = Field(title="Course coordinator")

    class Config:
        schema_extra = {
            "example": {
                "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                "name": "Engenharia Civil",
                "academic_unity": "CTEC",
                "coordinator": {
                    "key": "ecac7712-08e4-4bfb-a697-9018a74428fa",
                    "name": "Billy Jean",
                    "cpf": "000.000.000-01",
                }
            }
        }


class CourseDataInput(CourseDataBasic):
    coordinator_key: str = Field(title="Course coordinator key")

    class Config:
        schema_extra = {
            "example": {
                "name": "Engenharia Civil",
                "academic_unity": "CTEC",
                "creation_date": "2000-01-01",
                "coordinator_key":  "s3a9f393-3d58-4082-a8b8-cd89a63465fe"
            }
        }


class CourseList(BaseModel):
    __root__: List[CourseDataOutput]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'List of courses'
        }


class CourseListLite(BaseModel):
    __root__: List[CourseLite]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "title": 'Lite list of courses'
        }
